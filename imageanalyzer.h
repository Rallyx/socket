#ifndef IMAGEANALYSER_H
#define IMAGEANALYSER_H

#include <QObject>
#include <QWidget>

#include <QImage>
#include <QPixmap>

#include <opencv2/core.hpp>
#include <opencv/cvwimage.h>
#include <opencv2/highgui.hpp>
#include <cv.h>

#include <opencv2/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>

using namespace cv;

class ImageAnalyser
{
public:

    /**
     * @brief convert_to_qimage Convert an OpenCV Mat to a Qt QImage
     * @param mat the OpenCV Mat to convert
     * @return QImage converted from Mat
     */
    static QImage convert_to_qimage(const Mat mat);

    /**
     * @brief convert_to_mat Convert a QT Qimage to a OpenCV Mat
     * @param img the QT Qimage to convert
     * @return Mat converted from QImage
     */
    static Mat convert_to_mat(const QImage img);

    /**
     * @brief separate_images Splits an OpenCV Mat vertically
     * @param mat the OpenCV Mat to split
     * @param matL The adress of the left mat
     * @param matR The adress of the right mat
     */
    static void separate_images(const Mat mat, Mat &matL, Mat &matR);

    /**
     * @brief compute_laplacian Compute a Mat using the laplacian method
     * @param mat the OpenCV Mat to convert
     * @return the new converted image
     */
    static Mat compute_laplacian(const Mat mat);

    /**
     * @brief compute_sobel Compute a Mat using the sobel method
     * @param mat the OpenCV Mat to convert
     * @return the new converted image
     */
    static Mat compute_sobel(const Mat mat);

    /**
     * @brief invert_disparity_map Inverts a disparity map that is a OpenCV Mat (white to black instead of black to white)
     * @param mat the OpenCV Mat to convert
     * @return the new converted image
     */
    static Mat invert_disparity_map(const Mat mat);

    /**
     * @brief compute_disparity_map Makes a simple depth map without parameters
     * @param mat the OpenCV Mat to compute, will not be modified
     * @param inversed if the disparity map should be inversed
     * @return a new OpenCV Mat that represents the disparity map.
     */
    static Mat compute_disparity_map(const Mat mat, bool inversed);

    /**
     * @brief compute_advanced_disparity_map Makes an advanced depth map
     * @param mat the OpenCV Mat to compute, will not be modified
     * @param bmState the state of the StereoSGBM algorithm to use
     * @param inversed if the disparity map should be inversed
     * @return a new OpenCV Mat that represents the disparity map.
     */
    static Mat compute_advanced_disparity_map(const Mat mat, const cv::Ptr<cv::StereoSGBM> bmState, bool inversed);


    static Mat compute_depth_map(const Mat mat, double dist_cam, double focal_cam);

    static Mat compute_advanced_disparity_map( Mat LMat, Mat RMat, const Ptr<StereoSGBM> bmState, bool inversed);

};

#endif // IMAGEANALYSER_H
