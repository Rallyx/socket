#include "tcpclient.h"

using namespace std;

tcpclient::tcpclient()
{
    /*QTcpSocket * socket = (QTcpSocket*)malloc(sizeof(QTcpSocket));
    if(socket==NULL){
        std::cout << "Cannot allocate memory for socket." << std::endl;
    }
    this->client = socket;
    cout << "Socket created" << endl;*/
}

void tcpclient::connect(int port, QString host){
    QTcpSocket* socket = new QTcpSocket();
    cout << "try to connect..." << endl;
    socket->connectToHost(host, port);
    cout << ".. connected." << endl;
    this->client = socket;
}

void tcpclient::sendCmd(const char * cmd){
    this->client->write(cmd);
}

void tcpclient::rcvData(){
    this->data = this->client->readAll();
}

void tcpclient::getLeftImg(){
    const char * c_cmd = "IMG";
    this->sendCmd(c_cmd);
    this->rcvData();
}

cv::Mat tcpclient::loadLeftImage(){
    cv::Mat img = cv::Mat(480, 640, CV_8UC4, this->data.data());
    cv::cvtColor(img,img,cv::COLOR_BGRA2RGB);
    cv::imshow("Texture", img);
    return img;
}
