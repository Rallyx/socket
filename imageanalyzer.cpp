#include "imageanalyzer.h"

using namespace cv;
using namespace std;

void ImageAnalyser::separate_images(const Mat mat, Mat &matL, Mat &matR)
{
    // variables
    int offset = 0;
    // assertions
    if(mat.empty())
    {
        return;
    }
    // left img from otigin to src's middle
    matL = mat.colRange(0, mat.cols/2);
    // verify odd size
    offset = mat.cols % 2;
    matR = mat.colRange((mat.cols/2) + offset, mat.cols);
}

QImage ImageAnalyser::convert_to_qimage(const Mat mat)
{
    if(mat.empty())
    {
        return QImage();
    }

    // convert to rgb format
    Mat tmp;
    //mat.convertTo(mat, CV_16U);
    cvtColor(mat, tmp, CV_BGRA2RGBA);
    // make QImage
    QImage dest((const uchar *) tmp.data, tmp.cols, tmp.rows, tmp.step, QImage::Format_RGBA8888_Premultiplied);
    dest.bits(); // copy to avoid memory share on same array
    return dest;
}

Mat ImageAnalyser::convert_to_mat(const QImage img)
{
    Mat result;
    if(img.isNull())
    {
        return result;
    }
    QImage conv = img.convertToFormat(QImage::Format_RGBA8888_Premultiplied);
    Mat tmp(conv.height(),conv.width(),CV_8UC4,(void *)conv.constBits(), conv.bytesPerLine());
    cvtColor(tmp, result, COLOR_RGBA2BGRA);

    return result.clone();
}

Mat ImageAnalyser::compute_laplacian(const Mat mat)
{
    // init variable and convert img
    Mat gray, dest, abs_dst;
    Mat src = mat.clone();
    ///Transform to Gray and smooth
    GaussianBlur( src, src, Size(3,3), 0, 0, BORDER_DEFAULT );
    cvtColor( src, gray, CV_BGRA2GRAY );

    /// Apply Laplace function
    Laplacian( gray, dest, CV_16S, 3, 1, 0, BORDER_DEFAULT );
    convertScaleAbs( dest, abs_dst );

    ///Conversion for QImage
    cvtColor( abs_dst, abs_dst, CV_GRAY2BGRA );
    return abs_dst;
}

Mat ImageAnalyser::compute_sobel(const Mat mat)
{
    // init variable and convert img
    Mat src_gray;
    Mat src = mat.clone();
    ///Transform to Gray and smooth
    GaussianBlur( src, src, Size(3,3), 0, 0, BORDER_DEFAULT );
    cvtColor( src, src_gray, CV_BGRA2GRAY );

    /// Generate grad_x and grad_y
    Mat grad_x, grad_y, grad;
    Mat abs_grad_x, abs_grad_y;

    /// Gradient X
    Sobel( src_gray, grad_x, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x ); // convert back to CV_8U

    /// Gradient Y
    Sobel( src_gray, grad_y, CV_16S, 0, 1, 3, 1, 0, BORDER_DEFAULT );
    convertScaleAbs( grad_y, abs_grad_y ); // convert back to CV_8U

    /// Total Gradient (approximate)
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

    ///Conversion for QImage
    cvtColor( grad, grad, CV_GRAY2BGRA );

    return grad;
}

Mat ImageAnalyser::invert_disparity_map(const Mat mat)
{
    Mat returnedMat = mat.clone();

    //Bitwise_not to invert the images
    bitwise_not(returnedMat, returnedMat);

    //Loop through the images find all white pixels and replace with black
    for (int i = 0; i < returnedMat.rows; i++) {
        for (int j = 0; j < returnedMat.cols; j++) {
            if (returnedMat.at<uchar>(i, j) > 254) {
                returnedMat.at<uchar>(i, j) = 0;
            }
        }
    }

    return returnedMat;
}

Mat ImageAnalyser::compute_disparity_map(const Mat mat, bool inversed)
{
    Mat lMat, rMat, disparityMap, returnedMap;
    Ptr<StereoBM> bmState = StereoBM::create(CV_STEREO_BM_BASIC);

    // get img and separate in two parts
    Mat sourceMat = mat.clone();
    ImageAnalyser::separate_images(sourceMat, lMat, rMat);
    // get grey img
    cvtColor(lMat, lMat, CV_BGR2GRAY);
    cvtColor(rMat, rMat, CV_BGR2GRAY);
    // get disparity map
    //bmState.operator()(lMat, rMat, disparityMap);
    bmState->compute(lMat, rMat, disparityMap);
    normalize(disparityMap, returnedMap, 0, 255, CV_MINMAX, CV_8U);

    if(inversed) {
        returnedMap = invert_disparity_map(returnedMap);
    }

    return returnedMap;
}

Mat ImageAnalyser::compute_advanced_disparity_map(const Mat mat, const Ptr<StereoSGBM> bmState, bool inversed)
{
    Mat lMat, rMat, disparityMap, returnedMap;

    // get img and separate in two parts
    Mat sourceMat = ImageAnalyser::compute_sobel(mat);
    ImageAnalyser::separate_images(sourceMat, lMat, rMat);
    // get grey img
    cout << "ok" << endl;
    cvtColor(lMat, lMat, CV_BGR2GRAY);
    cvtColor(rMat, rMat, CV_BGR2GRAY);
    cout << "ok2" << endl;
    // get disparity map
    bmState->compute(lMat, rMat, disparityMap);
    normalize(disparityMap, returnedMap, 0, 255, CV_MINMAX, CV_8U);

    if(inversed) {
        returnedMap = invert_disparity_map(returnedMap);
    }

    return returnedMap;
}

Mat ImageAnalyser::compute_advanced_disparity_map(Mat LMat, Mat RMat, const Ptr<StereoSGBM> bmState, bool inversed)
{
    Mat  disparityMap, returnedMap;

    // get grey img
    cvtColor(LMat, LMat, CV_BGR2GRAY);
    cvtColor(RMat, RMat, CV_BGR2GRAY);
    // get disparity map
    bmState->compute(LMat, RMat, disparityMap);
    normalize(disparityMap, returnedMap, 0, 255, CV_MINMAX, CV_16S);
    if(inversed) {
        returnedMap = invert_disparity_map(returnedMap);
    }

    return returnedMap;
}

Mat ImageAnalyser::compute_depth_map(const Mat mat, double dist_cam, double focal_cam) {
    Mat depthMap;
    Mat sourceMat = ImageAnalyser::compute_disparity_map(mat, true);

    double disparity = 0;
    int diff = 0;
    for(int x = 0; x < sourceMat.cols; x++) {
        for(int y = 0; y < sourceMat.rows; y++) {
            diff = 255 - sourceMat.at<uchar>(x, y);
            disparity = (dist_cam * focal_cam) / diff;
            depthMap.at<uchar>(x,y) = disparity;
        }
    }

    return depthMap;
}

