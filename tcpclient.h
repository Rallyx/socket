#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QTcpSocket>
#include <QByteArray>
#include <opencv2/opencv.hpp>

class tcpclient
{
public:
    tcpclient();
    //
    void connect(int port, QString host);
    // receiver and sender throught TCP
    void sendCmd(const char * cmd);
    void rcvData();
    // left image handlers
    void getLeftImg();
    cv::Mat loadLeftImage();

    // socket
    QTcpSocket * client = nullptr;
    // Data buffer
    QByteArray data;
};

#endif // TCPCLIENT_H
