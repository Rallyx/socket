#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->client.connect(13000, "127.0.0.1");
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionOpen_triggered()
{
    // get filename of image
    // open dialog file box from QT
    QString filename = QFileDialog::getOpenFileName(this, "openWindowName", "~/", "Image Files (*.png *.jpg *.bmp *.jpeg)");

    // load image
    QImage img;
    img.load(filename);

    // set img on label
    // get image's size parameters
    int w = img.width();
    int h = img.height();
    // resize window
    this->resize(w,h);
    ui->imageLabel->resize(w,h);
    this->setFixedSize(w,h);
    // show img
    ui->imageLabel->setPixmap(QPixmap::fromImage(img));
    ui->imageLabel->show();
}

void MainWindow::on_actionGet_left_image_triggered()
{
    this->client.getLeftImg();
    cv::Mat m_img = this->client.loadLeftImage();
    QImage img = ImageAnalyser::convert_to_qimage(m_img);
    // set img on label
    // get image's size parameters
    int w = img.width();
    int h = img.height();
    std::cout << "Image(" << w << ", " << h << ")" << std::endl;
    // resize window
    this->resize(w,h);
    ui->imageLabel->resize(w,h);
    this->setFixedSize(w,h);
    // show img
    ui->imageLabel->setPixmap(QPixmap::fromImage(img));
    ui->imageLabel->show();

}
